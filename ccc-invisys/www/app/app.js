﻿var app = angular.module('ccc-invisys', ['onsen', 'ngCordovaOauth']);

app.controller('mainController', ['$scope', '$http', '$filter', '$cordovaOauth', function($scope, $http, $filter, $cordovaOauth) {
    $(".se-pre-con").fadeOut("fast");

    var SPREAD_SHEET_KEY = '19rSNahGoBbwOcRZtVhGwDmsfqshx68M59k30lZnKTL4';
    $scope.searchItem = '';
    $scope.items = [];
    $scope.users = [];
    $scope.selItem = -1;
    $scope.andVer = '';
    $scope.catfilter = [];
    $scope.toClose = false;
    $scope.upPW = false;

    ons.ready(function() {
        ons.setDefaultDeviceBackButtonListener(function() {
            if (navi.topPage.pushedOptions.page == "rootX.html" || navi.topPage.pushedOptions.page == "login.html") {
                if ($scope.toClose == false) {
                    window.plugins.toast.showShortBottom("Press the back button again to close app.");
                    $scope.toClose = true;
                    setTimeout(function() { $scope.toClose = false }, 3000);
                } else {
                    navigator.app.exitApp();
                }
            } else {
                navigator.app.backHistory()
            }
        });

        $(".se-pre-con").fadeOut("normal");
        $scope.googleSignIn = function(user) {
            // $scope.getAssetData();

            // 
            $cordovaOauth.google("596709900412-qino6ib56frlgnvfc43j8qbabrmmisfp.apps.googleusercontent.com", ["email"], {
                clear_cache: false,
                clear_session: false,
                prompt: true
            }).then(function(result) {
                $scope.getUserData(result);
                // console.log("Response Object -> " + JSON.stringify(result));
            }, function(error) {
                navi.resetToPage("login.html");
                $(".se-pre-con").fadeOut("normal");
                alert("Cannot sign-in. Please check your data connection.");
            });
        }

        $scope.googleSignOut = function() {
            window.cache.clear(function() {
                navi.resetToPage("login.html");
            });
            // window.open("https://mail.google.com/mail/logout?hl=en", "_blank", "location=no,clearsessioncache=yes");
        }

        $scope.getAndroidVersion = function(ua) {
            ua = (ua || navigator.userAgent).toLowerCase();
            var match = ua.match(/android\s([0-9\.]*)/);
            $scope.andVer = match ? match[1] : false;
        }
        $scope.fixtopBar = function() {
            $scope.getAndroidVersion();
            if ($scope.andVer >= 5) {
                console.log("5 above");
                document.querySelector('.navigation-bar--material__center').style.marginTop = "7px";
                document.querySelector('.navigation-bar--material__left').style.marginTop = "7px";
                document.querySelector('.navigation-bar--material__right').style.marginTop = "7px";
                document.querySelector('.navigation-bar__center').style.marginTop = "7px";
                document.querySelector('.navigation-bar__left').style.marginTop = "7px";
                document.querySelector('.navigation-bar__right').style.marginTop = "7px";
                document.querySelector('.button--material').style.paddingTop = "4px";
                document.querySelector('.button').style.paddingTop = "4px";
                document.querySelector('.ons-icon--ion').style.verticalAlign = "0";
            }
        }

        $scope.getAssetData = function() {
            $(".se-pre-con").show();
            navi.resetToPage("rootX.html");
            $http({
                method: "GET",
                url: 'https://spreadsheets.google.com/feeds/list/' + SPREAD_SHEET_KEY + '/1/public/basic?alt=json',
                type: 'jsonp'
            }).then(function Success(response) {
                    $scope.items = [];
                    var marker = ['idx', 'catx', 'typex', 'mfgx', 'modelx', 'colorx', 'serialx', 'imgx', 'userx', 's1x', 'lidx', 'pwx', 'notesx', 's2x', 'hostx', 'macadx', 'ipx', 'subnetx', 's3x', 'osx', 'cpux', 'hddx', 'ramx', 'odx', 'gcx', 'wifix'];
                    var raw = '';
                    var obj = response.data.feed.entry;
                    for (var i = 0; i < obj.length; i++) {
                        raw = obj[i].content.$t;
                        for (var j = 0; j < marker.length; j++) {
                            if (raw != '') {
                                raw = raw.replace(marker[j] + ': ', '"' + marker[j] + '": "');
                                raw = raw.replace(', "' + marker[j] + '":', '", "' + marker[j] + '":');
                            }
                        }
                        if (raw != '') {
                            $scope.items.push(JSON.parse('{' + raw + '"}'));
                            $scope.items[i].imgx = "images/CAT" + $scope.items[i].imgx + ".jpg";
                        }
                    }
                    $(".page__content").animate({ scrollTop: 0 }, "normal");
                    $(".se-pre-con").fadeOut("normal");
                },
                function Error(response) {
                    navi.resetToPage("login.html");
                    $(".se-pre-con").fadeOut("normal");
                    alert("Inventory Data Sheet cannot be loaded. Please check your data connection.");
                });
        }

        $scope.getUserData = function(result) {
            $(".se-pre-con").show();
            $http.get("https://www.googleapis.com/plus/v1/people/me", { params: { access_token: result.access_token } })
                .then(function(res) {
                    console.log("res: " + JSON.stringify(res));

                    $http({
                        method: "GET",
                        url: 'https://spreadsheets.google.com/feeds/list/' + SPREAD_SHEET_KEY + '/2/public/basic?alt=json',
                        type: 'jsonp'
                    }).then(function Success(response) {
                            $scope.items = [];
                            var marker = ['emailx', 'idx', 'pwx', 'logx'];
                            var raw = '';
                            var obj = response.data.feed.entry;
                            for (var i = 0; i < obj.length; i++) {
                                raw = obj[i].content.$t;
                                for (var j = 0; j < marker.length; j++) {
                                    if (raw != '') {
                                        raw = raw.replace(marker[j] + ': ', '"' + marker[j] + '": "');
                                        raw = raw.replace(', "' + marker[j] + '":', '", "' + marker[j] + '":');
                                    }
                                }
                                if (raw != '') {
                                    $scope.users.push(JSON.parse('{' + raw + '"}'));
                                }
                            }
                            var userVerified = $filter('filter')($scope.users, function(d) {
                                if (d.emailx == res.data.emails[0].value) {
                                    return true;
                                } else {
                                    return false;
                                }
                            });
                            console.log("users: " + JSON.stringify($scope.users[0]));
                            if (userVerified) {
                                console.log("User in sheet.")
                                $scope.getAssetData();
                            } else {
                                console.log("User not in sheet.")
                                navi.resetToPage("login.html");
                                $(".se-pre-con").fadeOut("normal");
                                alert("You are not authorized to access this app. Please contact your System Admin.");
                            }

                            //Verify user is in the USER worksheet.
                            //If wrong. Prompt appropriate message. 
                            //If correct. getAssetData()

                        },
                        function Error(response) {
                            navi.resetToPage("login.html");
                            $(".se-pre-con").fadeOut("normal");
                            alert("Inventory Data Sheet cannot be loaded. Please check your data connection.");
                        });

                }, function(error) {
                    navi.resetToPage("login.html");
                    $(".se-pre-con").fadeOut("normal");
                    alert("Inventory Data Sheet cannot be loaded. Please check your data connection.");
                })
        }

        $scope.scanQR = function() {
            cordova.plugins.barcodeScanner.scan(function(result) {
                var itemIndex = $filter('filter')($scope.items, function(d) {
                    if (d.idx == result.text) {
                        $scope.selItem = $scope.items[$scope.items.indexOf(d)];
                        document.querySelector('#mainNav').pushPage('item.html');
                    }
                })[0];
            });
        }
        $scope.openManageUser = function(){
            document.querySelector('#mainNav').pushPage('manageUsers.html');
        }
        $scope.openItem = function(itemID) {
            console.log(itemID);
            $filter('filter')($scope.items, function(d) {
                if (d.idx == itemID) {
                    $scope.selItem = $scope.items[$scope.items.indexOf(d)];
                    document.querySelector('#mainNav').pushPage('item.html');
                }
            });
        }
        $scope.closeItem = function() {
            $(".page__content").animate({ scrollTop: 0 }, "normal");
        }
        $scope.showNotesDialog = function() {
            document.getElementById('notes-dialog').show();
        }
        $scope.hideNotesDialog = function() {
            document.getElementById('notes-dialog').hide();
        }
        $scope.toggleFilter = function(value) {
            if ($scope.catfilter.length == 0) {
                $scope.catfilter.push(value);
            } else {
                if ($scope.catfilter.indexOf(value) > -1) {
                    $scope.catfilter.splice($scope.catfilter.indexOf(value), 1);
                } else {
                    $scope.catfilter.push(value);
                }
            }
            console.log($scope.catfilter);
        }

        $scope.fixtopBar();
        // $scope.getAssetData();
    });
}]);
